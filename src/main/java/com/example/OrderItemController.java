package com.example;

import static com.example.util.FluxUtils.wrapNotFoundIfEmpty;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/order-items")
public class OrderItemController {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final OrderItemRepository repository;
  private final OrderRepository orderRepository;
  private final ProductRepository productRepository;

  public OrderItemController(OrderItemRepository repository, OrderRepository orderRepository, ProductRepository productRepository) {
    this.repository = repository;
    this.orderRepository = orderRepository;
    this.productRepository = productRepository;
  }

  @GetMapping()
  public Flux<OrderItem> listAll() {
    logger.debug("listAll()");
    return repository.findAll();
  }

  @GetMapping("/{id}")
  public Mono<OrderItem> getOne(@PathVariable Long id) {
    logger.debug("getOne({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "orderItem", id);
  }

  @DeleteMapping("/{id}")
  public Mono<OrderItem> delete(@PathVariable Long id) {
    logger.debug("delete({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id)
        .flatMap(orderItem -> repository.delete(orderItem).thenReturn(orderItem)), "orderItem", id);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<OrderItem> create(@RequestBody @Valid OrderItem orderItem) {
    logger.debug("create({})", orderItem);
    return repository.save(orderItem);
  }

  @PutMapping("/{id}")
  public Mono<OrderItem> update(@PathVariable Long id, @RequestBody @Valid OrderItem entity) {
    logger.debug("create({} : {})", id, entity);
    entity.setId(id);
    return wrapNotFoundIfEmpty(repository.save(entity), "orderItem", id);
  }
  
  @GetMapping("/{id}/order")
  public Mono<Order> getOrder(@PathVariable Long id) {
    logger.debug("getOrder({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "order", id)
        .flatMap(order -> {
          Long orderId = order.getOrderId();
          return wrapNotFoundIfEmpty(orderRepository.findById(orderId), "order", orderId);
        });
  }
  
  @GetMapping("/{id}/product")
  public Mono<Product> getProduct(@PathVariable Long id) {
    logger.debug("getProduct({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "order", id)
        .flatMap(order -> {
          Long productId = order.getProductId();
          return wrapNotFoundIfEmpty(productRepository.findById(productId), "product", productId);
        });
  }
  
  
}
