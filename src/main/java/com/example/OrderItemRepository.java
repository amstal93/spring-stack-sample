package com.example;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface OrderItemRepository extends ReactiveCrudRepository<OrderItem, Long> {
  Flux<OrderItem> findByOrderId(Long orderId);
  Flux<OrderItem> findByProductId(Long productId);
}
