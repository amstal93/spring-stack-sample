package com.example;

import static com.example.util.FluxUtils.wrapNotFoundIfEmpty;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/products")
public class ProductController {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final ProductRepository repository;
  private final OrderItemRepository ordersRepository;

  public ProductController(ProductRepository repository, OrderItemRepository ordersRepository) {
    this.repository = repository;
    this.ordersRepository = ordersRepository;
  }

  @GetMapping()
  public Flux<Product> listAll() {
    logger.debug("listAll()");
    return repository.findAll();
  }

  @GetMapping("/{id}")
  public Mono<Product> getOne(@PathVariable Long id) {
    logger.debug("getOne({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "product", id);
  }

  @DeleteMapping("/{id}")
  public Mono<Product> delete(@PathVariable Long id) {
    logger.debug("delete({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id)
        .flatMap(product -> repository.delete(product).thenReturn(product)), "product", id);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Product> create(@RequestBody @Valid Product product) {
    logger.debug("create({})", product);
    return repository.save(product);
  }

  @PutMapping("/{id}")
  public Mono<Product> update(@PathVariable Long id, @RequestBody @Valid Product entity) {
    logger.debug("create({} : {})", id, entity);
    entity.setId(id);
    return wrapNotFoundIfEmpty(repository.save(entity), "product", id);
  }
  
  
  @GetMapping("/{id}/orders")
  public Flux<OrderItem> listAllOrders(@PathVariable Long id) {
    logger.debug("listAllOrders({})", id);
    return wrapNotFoundIfEmpty(getOne(id), "product", id)
        .flatMapMany(product -> ordersRepository.findByProductId(product.getId()));
  }

  @GetMapping("/{id}/orders/{ordersId}")
  public Mono<OrderItem> getOneOrders(@PathVariable Long id, @PathVariable Long ordersId) {
    logger.debug("getOneOrders({}, {})", id, ordersId);
    return wrapNotFoundIfEmpty(getOne(id), "product", id)
        .flatMap(product -> wrapNotFoundIfEmpty(ordersRepository.findById(ordersId), "orders", ordersId));
  }

  @DeleteMapping("/{id}/orders/{ordersId}")
  public Mono<OrderItem> deleteOrders(@PathVariable Long id, @PathVariable Long ordersId) {
    logger.debug("deleteOrders({}, {})", id, ordersId);
    return wrapNotFoundIfEmpty(getOne(id), "product", id)
        .flatMap(product -> wrapNotFoundIfEmpty(ordersRepository.findById(ordersId), "orders", ordersId))
        .flatMap(orders -> ordersRepository.delete(orders).thenReturn(orders));
  }

  @PostMapping("/{id}/orders")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<OrderItem> createOrders(@PathVariable Long id, @RequestBody @Valid OrderItem orders) {
    logger.debug("createOrders({}, {})", id, orders);
    return wrapNotFoundIfEmpty(getOne(id), "product", id)
        .flatMap(product -> {
          orders.setProductId(product.getId());
          return ordersRepository.save(orders);
        });
  }

  @PutMapping("/{id}/orders/{ordersId}")
  public Mono<OrderItem> updateOrders(@PathVariable Long id, @PathVariable Long ordersId, @RequestBody @Valid OrderItem orders) {
    logger.debug("updateOrders({}, {}, {})", id, ordersId, orders);
    return wrapNotFoundIfEmpty(getOne(id), "product", id)
        .flatMap(product -> {
          orders.setId(ordersId);
          return wrapNotFoundIfEmpty(ordersRepository.save(orders), "orders", id);
        });
  }
  
}
