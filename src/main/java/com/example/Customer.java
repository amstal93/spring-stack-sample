package com.example;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;


@Table("T_CUSTOMER")
public class Customer {

  @Id
  private Long id;
  
  @NotNull
  @Column("NAME")
  @JsonProperty
  private String name;

  public Long getId() {
    return id;
  }

  
  public String getName() { return name; }

  public void setId(Long id) {
    this.id = id;
  }

  
  public void setName(String name) { this.name = name; }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Customer customer = (Customer) o;
    return Objects.equals(id, customer.id)
        && Objects.equals(name, customer.name);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Customer{");
    sb.append("id=").append(id);
    sb.append(", name=").append(name);
    sb.append('}');
    return sb.toString();
  }

}
